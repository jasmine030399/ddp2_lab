import karyawan.*;
import java.util.ArrayList;
public class Korporasi{
	ArrayList<Karyawan> karyawan = new ArrayList<Karyawan>();
	
	public Karyawan find(String orang){
		Karyawan pekerja = null;
		for(int i = 0; i < karyawan.size(); i++){
			if (karyawan.get(i).getNama().equals(orang)){
				pekerja = karyawan.get(i);
				break;
			}
		}
		return pekerja;
	} 
	
	public String addpekerja(String orang, String posisi, int gaji, int batas){
		if(karyawan.size() > 1000){
			return "jumlah karyawan tidak bisa lebih dari 1000";
		}else{
			if (find(orang) != null){
				return "Karyawan dengan nama " + orang + " telah terdaftar di PT. TAMPAN";
			}else{
				if(posisi.equals("MANAGER")){
					Manager makeManager = new Manager(orang, gaji);
					karyawan.add(makeManager);
				}else if(posisi.equals("STAFF")){
					if(gaji > batas){
						return "batas maksimal gaji staff adalah " + batas;
					}else{
						Staff makeStaff = new Staff(orang, gaji);
						karyawan.add(makeStaff);
					}
				}else if(posisi.equals("INTERN")){
					Intern makeIntern = new Intern(orang, gaji);
					karyawan.add(makeIntern);
				}
			return orang + " mulai bekerja sebagai " + posisi + " di PT. TAMPAN";
			}
		}
	}
	
	public String status(String orang){
		if(find(orang) == null){
			return orang + " tidak ditemukan";
		}else{
			return orang + " " + find(orang).getGaji();
		}
	}
	
	public String rekrut(String meName, String recrutName){
		Karyawan perekrut = find(meName);
		Karyawan direkrut = find(recrutName);
		
		if(perekrut == null || direkrut == null){
			return "nama tidak berhasil ditemukan";
		}else{
			if(perekrut.canRekrut(direkrut) == true){
				for(int i = 0; i < perekrut.getList_rekrut().size(); i++){
					if(recrutName.equals(perekrut.getList_rekrut().get(i))){
						return "Karyawan " + recrutName + " telah menjadi bawahan " + meName; 
					}
				}
				perekrut.addrekrut(direkrut.getNama());
				return "Karyawan " + recrutName + " berhasil ditambahkan menjadi bawahan " + meName;
				
			}else{
				return "Anda tidak layak memiliki bawahan";
			}
		}
		

	}
	
	public String gajian(int batas){
		String output = "Semua karyawan telah diberikan gaji ";
		for(int i = 0 ; i< karyawan.size() ; i++){
			Karyawan pekerja = karyawan.get(i);
			pekerja.setJumlahgajian(pekerja.getJumlahgajian() + 1);
			if (find(pekerja.getNama()).getJumlahgajian() % 6 == 0){
				output += pekerja.getNama() + " mengalami kenaikan gaji sebesar 10% dari "+ pekerja.getGaji() +" menjadi "+ (int)(pekerja.getGaji()*1.1 )+"\n" ;
				pekerja.setGaji((int)(pekerja.getGaji() * 1.1));
				if(pekerja.getPosisi().equals("STAFF") && pekerja.getGaji() > batas){
					output += "\nSelamat, " + pekerja.getNama() + "telah dipromosikan menjadi MANAGER\n";
					pekerja.setPosisi("MANAGER");
				}
			}
		}
		return output;
	}
}
		
	
	