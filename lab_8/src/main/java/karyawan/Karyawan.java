package karyawan;
import java.util.ArrayList;
public class Karyawan{
	protected String nama;
	protected int gaji;
	protected String posisi;
	//protected boolean isBawahan;
	protected ArrayList<String> list_rekrut;
	protected int jumlahgajian = 0;
	
	public Karyawan(String nama, int gaji){
		this.nama = nama;
		this.gaji = gaji;
		this.posisi = posisi;
		this.list_rekrut = new ArrayList<String>();
		this.jumlahgajian = jumlahgajian;
	}
	
	public void setNama(String nama){ this.nama = nama;}
	public String getNama(){ return nama;}
	
	public void setGaji(int gaji){this.gaji = gaji;}
	public int getGaji(){return gaji;}
	
	public void setPosisi(String posisi){this.posisi = posisi;}
	public String getPosisi(){return posisi;}
	
	
	
	public ArrayList<String> getList_rekrut(){return list_rekrut;}
	
	public void addrekrut(String orang){
		list_rekrut.add(orang);
	} 
	
	public void setJumlahgajian(int jumlahgajian){this.jumlahgajian = jumlahgajian;}
	public int getJumlahgajian(){return jumlahgajian;}
	
	
	public boolean canRekrut(Karyawan pekerja){
		if(getPosisi().equals("MANAGER")){
			if(pekerja.getPosisi().equals("STAFF") || pekerja.getPosisi().equals("INTERN") && getList_rekrut().size() < 10){
				return true;
			}else{
				return false;
			}
		}else if(getPosisi().equals("STAFF")){
			if(pekerja.getPosisi().equals("INTERN") && getList_rekrut().size() < 10){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
		
		
	}

}
	
	
