import java.util.Scanner;
import karyawan.*;
class Lab8 {
    public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Korporasi kor = new Korporasi();
		
		int batas_gaji = Integer.parseInt(input.nextLine());
		
		while(true){
			String perintah = input.nextLine().toUpperCase();
			String[] masukan = perintah.split(" ");
			if(masukan[0].equals("TAMBAH_KARYAWAN")){
					System.out.println(kor.addpekerja(masukan[1],masukan[2],Integer.parseInt(masukan[3]),batas_gaji));
			}else if(masukan[0].equals("STATUS")){
				System.out.println(kor.status(masukan[1]));
			}else if(masukan[0].equals("TAMBAH_BAWAHAN")){
				System.out.println(kor.rekrut(masukan[1], masukan[2]));
			}else if(masukan[0].equals("GAJIAN")){
				System.out.println(kor.gajian(batas_gaji));
			}else{
				System.out.println("maaf perintah yang anda masukan salah");
			}
		}

    }
}