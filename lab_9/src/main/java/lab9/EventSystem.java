package lab9;

import lab9.user.User;
import lab9.event.Event;

import java.util.ArrayList;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
* Class representing event managing system
*/
public class EventSystem
{
    /**
    * List of events
    */
    private ArrayList<Event> events;
    
    /**
    * List of users
    */
    private ArrayList<User> users;
    
    /**
    * Constructor. Initializes events and users with empty lists.
    */
    public EventSystem()
    {
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }
    
	
    
    public String addUser(String name)
    {
		if(getUser(name) == null){
			User pengguna = new User(name);
			users.add(pengguna);
			return "User " + name +  " berhasil ditemukan!";
		}else{
			return "User " + name + " sudah ada!";
		}
        // TODO: Implement:
        
    }
	
	public User getUser(String name){
		for (User i : users){
            if (i.getName().equals(name)){ return i;}
        }
        return null;
    }
	
	public Event findEvent(String name){
		Event acara = null;
		for(int i = 0; i < events.size() ; i++){
			if (events.get(i).getName().equals(name)){
				acara = events.get(i);
				break;
			}
		}
		return acara;
	}
	
	public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr)
    {
        String tambahEvent = "";

        String[] mulai_split = startTimeStr.split("_");
        String[] tanggal_split = mulai_split[0].split("-");
        String[] waktu_split = mulai_split[1].split(":");

        String[] akhir_split = endTimeStr.split("_");
        String[] tanggal_split2 = akhir_split[0].split("-");
        String[] waktu_split2 = akhir_split[1].split(":");

        int tahunAwal = Integer.parseInt(tanggal_split[0]);
        int tahunAkhir = Integer.parseInt(tanggal_split2[0]);

        int bulanAwal = Integer.parseInt(tanggal_split[1]);
        int bulanAkhir = Integer.parseInt(tanggal_split2[1]);

        int hariAwal = Integer.parseInt(tanggal_split[2]);
        int hariAkhir = Integer.parseInt(tanggal_split2[2]);

        int jamAwal = Integer.parseInt(waktu_split[0]);
        int jamAkhir = Integer.parseInt(waktu_split2[0]);

        int menitAwal = Integer.parseInt(waktu_split[1]);
        int menitAkhir = Integer.parseInt(waktu_split2[1]);

        int detikAwal = Integer.parseInt(waktu_split[2]);
        int detikAkhir = Integer.parseInt(waktu_split2[2]);

        Date startDate = new Date(tahunAwal-1900,bulanAwal-1,hariAwal,jamAwal,menitAwal,detikAwal);
        Date endDate = new Date(tahunAkhir-1900,bulanAkhir-1,hariAkhir,jamAkhir,menitAkhir,detikAkhir);

        if (findEvent(name) != null){
            tambahEvent = "Event " + name + " sudah ada!";
        }
        else if (startDate.before(endDate)){
            Event eventAdd = new Event(name,startDate,endDate,costPerHourStr);
            events.add(eventAdd);
            tambahEvent = "Event " + name +" berhasil ditambahkan!";
        }
        else{
            tambahEvent = "Waktu yang diinputkan tidak valid!";
        }
        return tambahEvent;

    }


	public Event getEvent(String name){
        for (Event i : events){
            if (i.getName().equals(name)) return i;
        }
        return null;
    }

    
    public String registerToEvent(String namaPengguna, String namaEvents)
    {
       if (getUser(namaPengguna) == null & getEvent(namaEvents) == null){
            return "Tidak ada pengguna  dengan nama " + namaPengguna + " dan acara dengan nama " + namaEvents + "!";
        }
        if (getUser(namaPengguna) == null){
            return "Tidak ada pengguna dengan nama " + namaPengguna + "!";
        }
        if (getEvent(namaEvents) == null){
            return "Tidak ada acara dengan nama " + namaEvents + "!";
        }

        Event event = getEvent(namaEvents);
        User user = getUser(namaPengguna);

        if (user.addEvent(event) == true){
            return namaPengguna + " berencana menghadiri " + namaEvents + "!";          
        }
        return namaPengguna + " sibuk sehingga tidak dapat menghadiri " + namaEvents + "!";

    }
}