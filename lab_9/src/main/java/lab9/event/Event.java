package lab9.event;

import java.util.Date;
import java.math.BigInteger;
import java.text.SimpleDateFormat;

/**
* A class representing an event and its properties
*/
public class Event
{
    /** Name of event */
    private String name;
	/** instance variables for representing beginning and end time of event*/
	private Date waktumulai;
	private Date waktuselesai; 
	/**instance variables for cost per month*/
	private String costperhour;
	
	/**Create constructor for Event class*/
	public Event(String name, Date waktumulai, Date waktuselesai, String costperhour){
		this.name = name;
		this.waktumulai = waktumulai;
		this.waktuselesai = waktuselesai;
		this.costperhour = costperhour;
	}
    
  
    
    /**
    * Accessor for name field. 
    * @return name of this event instance
    */
    public String getName(){return this.name;}
	
	public Date getWaktumulai(){return this.waktumulai;}
		
	public Date getWaktuselesai(){return this.waktuselesai;}
	
	public BigInteger getCost(){
		BigInteger cost = new BigInteger(costperhour);
		return cost;
	}
	
	public String toString(){
		String info = "";
		String pattern = "dd-MM-YYYY, HH:mm:ss";
		SimpleDateFormat awalwaktu = new SimpleDateFormat(pattern);
		String awalWaktustr = awalwaktu.format(waktumulai);
		
		SimpleDateFormat akhirwaktu = new SimpleDateFormat(pattern);
		String akhirWaktustr = akhirwaktu.format(waktuselesai);
		
		info += name + "\nWaktu mulai : " + awalWaktustr + "\nWaktu selesai : " + akhirWaktustr + "\nBiaya kehadiran : " + costperhour;
		return info;
	}
	
	public boolean overlap(Event acara){
		if(acara.getWaktumulai().after(this.waktumulai) && acara.getWaktumulai().before(this.waktuselesai)){ //kalo overlap return true 
			return true;
		}else if(acara.getWaktuselesai().after(this.waktumulai) && acara.getWaktuselesai().before(this.waktuselesai)){
			return true;
		}else{
			return false;
		}
	}
	
	public int compareTo(Event acara){
		Date CompareWaktu = acara.getWaktumulai();
		return this.waktumulai.compareTo(CompareWaktu);
	}
}
