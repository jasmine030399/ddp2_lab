import character.*;
import java.util.ArrayList;

public class Game{
    ArrayList<Player> player = new ArrayList<Player>();
	ArrayList<String> charaDimakan = new ArrayList<String>(); 
    
    /**
     * Fungsi untuk mencari karakter
     * @param String name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String chara){
		Player pemain = null;
		for(int l = 0 ; l < player.size() ; l++){
			if(player.get(l).getName().equals(chara)){
				pemain = player.get(l);
				break;
			}
		}
		return pemain;
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp){
		return add(chara, tipe, hp, "AAAAAAaaaAAAAAaaaAAAAAA");
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @param String roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar){
		if(find(chara) != null){
			return "sudah ada karakter bernama " + chara;
		}else{
			if(tipe.equals("Monster")){
				Monster makeMonster = new Monster(chara, hp, roar);
				player.add(makeMonster);
			}else if(tipe.equals("Human")){
				Human makeHuman = new Human(chara, hp);
				player.add(makeHuman);
			}else{
				Magician makeMagician = new Magician(chara, hp);
				player.add(makeMagician);
			}
		return chara + " ditambah ke game";
		}
        
    }

    /**
     * fungsi untuk menghapus character dari game
     * @param String chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara){
		if(player.remove(find(chara))){
			return chara + " dihapus dari game";
		}else{
			return "tidak ada " + chara;
		}
	}


    /**
     * fungsi untuk menampilkan status character dari game
     * @param String chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */
    public String status(String chara){
		if(find(chara) == null){
			return "tidak ada " + chara;
		}else{
			Player siPenyerang = find(chara);
			String out = siPenyerang.getTipe() + " " + siPenyerang.getName() + "\n HP: " + siPenyerang.getHp();
			if(siPenyerang.getisDead() == true){
				out = out + "\nSudah meninggal dunia dengan damai\n";
			}else{
				out = out + "\nMasih hidup\n";
			}
			if(siPenyerang.getDiet().size() == 0){
				out = out + "Belum memakan siapa siapa";
			}else{
				out = out + "Memakan " + diet(chara);
			}
			return out;
		}
	}

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public String status(){
		String out = "";
		for(int i = 0; i < player.size() ; i++){
			out = out + status(player.get(i).getName()) + "\n";
		}
        return out;        
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     * @param String chara Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara){
		if(find(chara) == null){
			return "tidak ada " + chara;
        }else{
			Player siPenyerang = find(chara);
			if(siPenyerang.getDiet().size() == 0){
				return "";
			}else{
				String keluaran = "";
				for(int i = siPenyerang.getDiet().size() - 1; i >= 0; i--){
					if(i != 0){
						keluaran += siPenyerang.getDiet().get(i) + ", ";
					}else{
						keluaran += siPenyerang.getDiet().get(i);
					}}
					return keluaran;
	}}}

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet(){
		String out = "";
		ArrayList<String> eaten = new ArrayList<String>();
		for(int i = 0; i < player.size() ; i++){
			String nama = player.get(i).getName();
			String udhdimakan = diet(nama);
			if(!udhdimakan.equals("")){
				eaten.add(udhdimakan);
			}
		}
        out += "Termakan : " ;
		for(int i = 0; i < eaten.size(); i++){
			if(i != 0){
				out += eaten.get(i) + " , ";
			}else{
				out += eaten.get(i);
			}
		}
		return out;
	}
    

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName){
		Player siPenyerang = find(meName);
		Player siMusuh = find(enemyName);
		siPenyerang.attack(siMusuh);
		return "Nyawa " + enemyName + " " + siMusuh.getHp();
	}

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName){
		Player siPenyerang = find(meName);
		Player siMusuh = find(enemyName);
		String output = "";
		if(siPenyerang.getTipe().equals("Magician")){
			((Magician)siPenyerang).burn(siMusuh);
			output = output + "Nyawa " + enemyName + " " + siMusuh.getHp();
			if(siMusuh.getisBurn() == true){
				output = output + "\n dan matang";
			}
			return output;
		}else{
			return meName + " bukan Magician";
		}
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName){
		Player siPenyerang = find(meName);
		Player siMusuh = find(enemyName);
		if(siPenyerang.canEat(siMusuh) == true){
			siPenyerang.setHp(siPenyerang.getHp() + 15);
			siPenyerang.addDiet(siMusuh.getTipe() + " " + enemyName);
			charaDimakan.add(siMusuh.getTipe() + " " + enemyName);
			player.remove(siMusuh);
			return meName + " memakan " + enemyName + "\nNyawa " + meName + " kini " + siPenyerang.getHp();
		}else{
			return meName + " tidak bisa memakan " + enemyName;
		}
    }

     /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     * @param String meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName){
		if(find(meName) != null){
		   if (find(meName).getTipe().equals("Monster")){
			   Player siPenyerang = find(meName);
			   return ((Monster)siPenyerang).roar();
		   }else{
			   return meName + " tidak bisa berteriak";
		   }
		}
		return "tidak ada " + meName;	   
    }
}