package character;
public class Monster extends Player{
	private String roar;
	public Monster(String nama, int hp){
		super(nama, hp*2);
		this.tipe = "Monster";
		this.roar = "AAAAAAaaaAAAAAaaaAAAAAA";
		
	}
	
	public Monster(String nama, int hp, String roar){
		super(nama, hp*2);
		this.tipe = "Monster";
		this.roar = roar;
	}
	
	public String roar(){
		return roar;
	}
	
	
}