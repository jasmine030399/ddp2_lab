package character;
import java.util.ArrayList;
public class Player{
	protected String nama;
	protected int hp;
	protected ArrayList<String> diet; //player yang sudah dimakan
	protected String tipe;
	protected boolean isDead;
	protected boolean isBurn;
	public Player(String nama, int hp){
		this.nama = nama;
		this.hp = hp;
		if(this.hp <= 0){
			this.isDead = true;
		}
		this.diet = new ArrayList<String>();
	}
	
	
	public void setNama(String nama){ this.nama = nama;}
	public String getName(){return nama;}
	
	public void setHp(int hp){
		if (hp <= 0){
			this.hp = 0;
			setisDead(true);
		}else{
			this.hp = hp;
		}
	}
	public int getHp(){return hp;}
	
	public void setTipe(String tipe){this.tipe = tipe;}
	public String getTipe(){return tipe;}
	
	public void setDiet(ArrayList<String> diet){this.diet = new ArrayList<String>();}
	public ArrayList<String> getDiet(){return diet;}
	
	public void addDiet(String makanan){
		getDiet().add(makanan);
	}
	
	
	public void setisBurn(boolean isBurn){this.isBurn = isBurn;}
	public boolean getisBurn(){return isBurn;} //false
	
	public void setisDead(boolean isDead){this.isDead = isDead;}
	public boolean getisDead(){return isDead;}
	
	public boolean canEat(Player musuh){
		if(musuh.getTipe().equals("Monster")){ //pemainnya, musuhnya idup, musuhnya bisa dimakan
			if(getisDead() == false && musuh.getisDead() == true){
				return true;
			}else{
				return false;
			}
		}else if(musuh.getTipe().equals("Human")){
			if(musuh.getTipe().equals("Monster") && getisDead() == false && musuh.getisBurn() == true){
				return true;
			}else{
				return false;
			}
		}else{
			if(musuh.getTipe().equals("Monster") && getisDead() == false && musuh.getisBurn() == true){ //getisdead pemain
				return true;
			}else{
				return false;
			}
		}
	}
	
	public void attack(Player musuh){
		if(musuh.getTipe().equals("Magician")){
			musuh.setHp(musuh.getHp() - 20);
		}else{
			musuh.setHp(musuh.getHp() -10);
		}
	}	
}
	
	
	

//  write Player Class here
