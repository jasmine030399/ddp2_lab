package character;
public class Magician extends Player{
	public Magician(String nama, int hp){
		super(nama,hp);
		this.tipe = "Magician";
		
	}
	
	public void burn(Player musuh){
		if(musuh.getTipe().equals("Magician")){
			musuh.setHp(musuh.getHp() - 20);
		}else{
			musuh.setHp(musuh.getHp() -10);
		}
		if(musuh.getHp() <= 0){
			musuh.setisBurn(true); //true kebakar
		}
	}
}
//  write Magician Class here