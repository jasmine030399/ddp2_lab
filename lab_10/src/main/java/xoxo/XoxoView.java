package xoxo;

import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JFrame;
import java.awt.event.ActionListener;


import java.awt.event.ActionEvent;



import javax.swing.*;
import java.awt.*;
import xoxo.crypto.*;
import xoxo.exceptions.*;
import xoxo.key.*;
import xoxo.util.*;

/**
 * This class handles most of the GUI construction.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author <write your name here>
 */
public class XoxoView {
    
    /**
     * A field that used to be the input of the
     * message that wants to be encrypted/decrypted.
     */
    private JTextField messageField;

    /**
     * A field that used to be the input of the key string.
     * It is a Kiss Key if it is used as the encryption.
     * It is a Hug Key if it is used as the decryption.
     */
    private JTextField keyField;

    /**
     * A field that used to display any log information such
     * as you click the button, an output file succesfully
     * created, etc.
     */
    private JTextArea logField; 

    /**
     * A button that when it is clicked, it encrypts the message.
     */
    private JButton encryptButton;

    /**
     * A button that when it is clicked, it decrpyts the message.
     */
    private JButton decryptButton;

    //TODO: You may add more components here
	private JPanel textPanel, mainPanel, buttonPanel;
	private JTextField seedField;


    /**
     * Class constructor that initiates the GUI.
     */
    public XoxoView() {
        this.initGui();
    }

    /**
     * Constructs the GUI.
     */
    private void initGui() {
        //TODO: Construct your GUI here
		JFrame frame = new JFrame("xoxo Encryptor/Decryptor");
		encryptButton = new JButton("Encrypt");
		decryptButton = new JButton("Decrypt");
		messageField = new JTextField();
		keyField = new JTextField();
		logField = new JTextArea();
		
		textPanel = new JPanel();
        textPanel.setLayout(new GridLayout(3,3,0,5));

        mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());

        buttonPanel = new JPanel();
        buttonPanel.setLayout(new FlowLayout());

        frame = new JFrame("Secret message");
        frame.setSize(300,300);

        JLabel textLabel = new JLabel("Enter Message : ");
        textLabel.setHorizontalAlignment(textLabel.CENTER);
        textPanel.add(textLabel);
        textPanel.add(messageField);

        JLabel keyLabel = new JLabel("Enter Key : ");
        keyLabel.setHorizontalAlignment(keyLabel.CENTER);
        keyField = new JTextField();
        textPanel.add(keyLabel);
        textPanel.add(keyField);

        JLabel seedLabel = new JLabel("Enter Seed : ");
        seedLabel.setHorizontalAlignment(seedLabel.CENTER);
        seedField = new JTextField();
        textPanel.add(seedLabel);
        textPanel.add(seedField);

		
		buttonPanel.add(encryptButton);
        buttonPanel.add(decryptButton);
        

        mainPanel.add(textPanel,BorderLayout.PAGE_START);
        mainPanel.add(buttonPanel,BorderLayout.PAGE_END);

        
        mainPanel.add(logField,BorderLayout.CENTER);

        frame.setContentPane(mainPanel);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);   

		
		
    }

    /**
     * Gets the message from the message field.
     * 
     * @return The input message string.
     */
    public String getMessageText() {
        return messageField.getText();
    }

    /**
     * Gets the key text from the key field.
     * 
     * @return The input key string.
     */
    public String getKeyText() {
        return keyField.getText();
    }

    /**
     * Appends a log message to the log field.
     *
     * @param log The log message that wants to be
     *            appended to the log field.
     */
    public void appendLog(String log) {
        logField.append(log + '\n');
    }

    /**
     * Sets an ActionListener object that contains
     * the logic to encrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to encrypt a message.
     */
    public void setEncryptFunction(ActionListener listener) {
        encryptButton.addActionListener(listener);
    }
    
    /**
     * Sets an ActionListener object that contains
     * the logic to decrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to decrypt a message.
     */
    public void setDecryptFunction(ActionListener listener) {
        decryptButton.addActionListener(listener);
    }
	
	public String getSeedText() {
        return seedField.getText();
    }

}