package xoxo;
import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import xoxo.crypto.*;
import xoxo.exceptions.*;
import xoxo.key.*;
import xoxo.util.*;

/**
 * This class controls all the business
 * process and logic behind the program.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author <write your name here>
 */
public class XoxoController {

    /**
     * The GUI object that can be used to get
     * and show the data from and to users.
     */
    private XoxoView gui;

    /**
     * Class constructor given the GUI object.
     */
    public XoxoController(XoxoView gui) {
        this.gui = gui;
    }

    /**
     * Main method that runs all the business process.
     */
    public void run() {
        //TODO: Write your code for logic and everything here
		 ActionListener decryptAct = new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                XoxoDecryption xd = new XoxoDecryption(gui.getKeyText());
                String seed = gui.getSeedText();
                String result = "";
                if (seed.equalsIgnoreCase("DEFAULT_SEED")){
                    result =  xd.decrypt(gui.getMessageText(),18);
                }
                else{
                    int seedInt = Integer.parseInt(gui.getSeedText());
                    result =  xd.decrypt(gui.getMessageText(),seedInt);
                }
                gui.appendLog(result); 
            } 
        };
        this.gui.setDecryptFunction(decryptAct);

        ActionListener encryptAct = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                XoxoEncryption xe = new XoxoEncryption(gui.getKeyText());
                XoxoMessage xm;
                String seed = gui.getSeedText();
                if (seed.equalsIgnoreCase("DEFAULT_SEED")){
                    xm =  xe.encrypt(gui.getMessageText());
                }
                else{
                    int seedInt = Integer.parseInt(gui.getSeedText());
                    xm =  xe.encrypt(gui.getMessageText(),seedInt); 
                }
                gui.appendLog(xm.getEncryptedMessage()); 
            } 
        };
        this.gui.setEncryptFunction(encryptAct);
    }

}


    //TODO: Create any methods that you want