package xoxo.crypto;
import xoxo.exceptions.RangeExceededException;

/**
 * This class is used to create a decryption instance
 * that can be used to decrypt an encrypted message.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 */
public class XoxoDecryption {

    /**
     * A Hug Key string that is required to decrypt the message.
     */
    private String hugKeyString;

    /**
     * Class constructor with the given Hug Key string.
     */
    public XoxoDecryption(String hugKeyString) {
        this.hugKeyString = hugKeyString;
    }

    /**
     * Decrypts an encrypted message.
     * 
     * @param encryptedMessage An encrypted message that wants to be decrypted.
     * @return The original message before it is encrypted.
     */
    public String decrypt(String encryptedMessage, int seed) {
        //TODO: Implement decryption algorithm
		if(seed > 36|| seed < 0){
			throw new RangeExceededException("seed merupakan angka diantara 0-36(inclusive)");
			
		}
		final int panjang = encryptedMessage.length();
		String decryptMessage = "";
		for(int i = 0; i < panjang; i++){
			int m = this.hugKeyString.charAt(i % hugKeyString.length());
			int b = m ^seed;
            int k = b - 'a';
			int a = encryptedMessage.charAt(i)^k;
			
            decryptMessage += (char) a;
		
		}
		return decryptMessage;
        
    }
}